# -*- conding: utf-8 -*-
from .base import *

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False
SECURE_SSL_REDIRECT = False
DEBUG = True
